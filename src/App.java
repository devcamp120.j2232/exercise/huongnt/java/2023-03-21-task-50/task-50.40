
import java.util.Arrays;
import java.util.*;
import java.util.Collections;

public class App {
    public static void main(String[] args) throws Exception {

        // 1 Tạo một mảng gồm các phần tử có giá trị từ 1 đến y
        int n = 7;
        int[] arr = new int[n];
        Arrays.setAll(arr, i -> i + 1);

        System.out.println("Task 1:" + Arrays.toString(arr));

        // 1 Tạo một mảng gồm các phần tử có giá trị từ x đến y
        System.out.println("Enter Input N:");
        // Scanner sc = new Scanner(System.in);
        int n1 = 7;

        // Create Array of Size N
        int arr1[] = new int[7];
        for (int i = 3; i < n1; i++) {
            // we fill array with values starting from 1 to 10
            arr1[i] = i + 1;
        }

        System.out.println("Array with values x to " + n1);
        System.out.println(Arrays.toString(arr1));

        // 2 Gộp 2 mảng lại với nhau, bỏ hết các phần tử có giá trị trùng nhau

        int[] a = { 1, 2, 3 };
        int[] b = { 100, 2, 1, 10 };

        ArrayList<Integer> l = new ArrayList<>();
        for (int i = 0; i < (a.length > b.length ? a.length : b.length); i++) {
            if (i < a.length) {
                int c = 0;
                while (c <= l.size()) {
                    if (l.contains(a[i]) == false) {
                        l.add(a[i]);
                    }
                    c++;
                }
            }
            if (i < b.length) {
                int c = 0;
                while (c <= l.size()) {
                    if (l.contains(b[i]) == false) {
                        l.add(b[i]);
                    }
                    c++;
                }
            }
        }
        System.out.println("2:Merge two arrays and remove duplicates: " + l);

        // 3 Đếm số lần xuất hiện của phần tử n trong mảng
        int arr3[] = new int[] { 1, 2, 1, 4, 5, 1, 1, 3, 1 };
        int n3 = arr.length;
        countFreq(arr3, n3);

        // 4. Tính tổng các phần tử trong mảng

        // Initialize array
        int[] arr4 = new int[] { 1, 2, 3, 4, 5, 6 };
        int sum = 0;
        // Loop through the array to calculate sum of elements
        for (int i = 0; i < arr4.length; i++) {
            sum = sum + arr4[i];
        }
        System.out.println("4. Sum of all the elements of an array: " + sum);

        // 5 Từ một mảng cho trước, tạo một mảng gồm toàn các số chẵn
        ArrayList evens = new ArrayList();
        int[] simpleTable = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        for (int i : simpleTable) {
            if (i % 2 == 0) {
                evens.add(i);
            }
        }
        System.out.println("5. Even array list: " + evens);

        // 6. Find sum of two array elements
        // Declare and initialize the array elements
        int[] a6 = { 1, 0, 2, 3, 4 };
        int[] b6 = { 3, 5, 6, 7, 8, 13 };

        // get length of an array and store it in c array
        int[] c6 = new int[b6.length];

        // check if length of both array are equal
        // if(a6.length==b6.length){
        // logic implementation
        for (int i = 0, j = 0, k = 0; i < a6.length; i++, j++, k++) {
            c6[k] = 0;
            if (i > a6.length) {
                c6[k] = 0 + b6[j];
            } else {
                c6[k] = a6[i] + b6[j];
            }

        }

        // Print the result
        System.out.println("6. Resultant array is:");
        System.out.println(Arrays.toString(c6));
        // } else {
        // System.out.println("Length of both array should be same");
        // }

        // 8
        int a8[] = { 1, 2, 3 };
        int b8[] = { 100, 2, 1, 10 };
        int n8 = a8.length;
        int m8 = b8.length;
        int res8[] = new int[n8 + m8];
        SortArray(a8, b8, res8, n8, m8);
        for (int i = 0; i < n8 + m8; i++)
            ;
        int h8 = res8.length;
        h8 = removeDuplicates(res8, h8);

        System.out.println("8. Printing element after removing duplicates");
        for (int i = 0; i < h8; i++) {
            System.out.println(res8[i]);
        }

        // 9. Sắp xếp các phần tử theo giá trị giảm dần
        Integer[] arr9 = { 1,3,1,4,2,5,6};
        Arrays.sort(arr9, Collections.reverseOrder());
        System.out.println("9. Sort an Array in Des Order: ");
        for (int values9 : arr9) {
            System.out.print(values9 + ", ");
        }

        // 9. Sort tăng dần
        int[] arr92 = { 1, 3, 1, 4, 2, 5, 6 };
        Arrays.sort(arr92);
        System.out.println("\n"+"9.2. Sort an Array in Ascending Order: ");
        for (int values : arr92) {
            System.out.println(values + ", ");
        }

        //10.
        int  [] array10 = {10, 20, 30, 40, 50};
        int n10 = 2;
        System.out.println("Array " + Arrays.toString(array10));
        ShiftToRight(array10,n10);
    

    }

    // 3
    public static void countFreq(int arr[], int n) {
        boolean visited[] = new boolean[n];

        Arrays.fill(visited, false);

        // Traverse through array elements and
        // count frequencies
        for (int i = 0; i < n; i++) {

            // Skip this element if already processed
            if (visited[i] == true)
                continue;

            // Count frequency
            int count = 1;
            for (int j = i + 1; j < n; j++) {
                if (arr[i] == arr[j]) {
                    visited[j] = true;
                    count++;
                }
            }
            System.out.println("3. Counting frequencies of array elements: " + arr[i] + " " + count);
        }
    }

    // 8
    public static void SortArray(int a[], int b[], int res[], int n, int m) {
        int i = 0, j = 0, k = 0;
        while (i < n) {
            res[k] = a[i];
            i++;
            k++;
        }
        while (j < m) {
            res[k] = b[j];
            j++;
            k++;
        }
        Arrays.sort(res);

    }

    static int removeDuplicates(int res[], int a) {

        if (a == 0 || a == 1) {
            return a;
        }

        int j = 0;
        for (int i = 0; i < a - 1; i++)
            if (res[i] != res[i + 1])
                res[j++] = res[i];
        res[j++] = res[a - 1];

        return j;}


        //10
        public static void ShiftToRight(int a[],int n){
            int temp = a[n];
    
            for (int i = n; i > 0; i--) {
                a[i] = a[i-1];
            }
    
            a[0] = temp;
    
            System.out.println("10. Array after shifting to right by one : " + Arrays.toString(a));
    
    }


}